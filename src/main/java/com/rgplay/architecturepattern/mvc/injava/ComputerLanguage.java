package com.rgplay.architecturepattern.mvc.injava;

/*
 This is a Model class in this MVC architecture pattern example
 */
public class ComputerLanguage {
    String language;
    String mobilePlatform;

    ComputerLanguage (String language, String mobilePlatform){
        this.language = language;
        this.mobilePlatform = mobilePlatform;
    }

    String getLanguage(){
        return this.language;
    }

    String getMobilePlatform(){
        return this.mobilePlatform;
    }
}
