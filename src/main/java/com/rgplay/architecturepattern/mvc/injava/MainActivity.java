package com.rgplay.architecturepattern.mvc.injava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
 Activity plays dual role as a View and also as the Controller.
 As a View, it has acess to all elements of View.
 As a Controller, it talks to Datasource and populate Models. And tie the controller method to the view event.
 */

public class MainActivity extends AppCompatActivity {

    TextView tvDisplayArea;
    Button btClickMeToDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDisplayArea = findViewById(R.id.tvDisplayArea);
        btClickMeToDisplay = findViewById(R.id.btClickMeToDisplay);

        btClickMeToDisplay.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Invoking displayText() controller method
                        displayText();
                    }
                }
        );

    }

    /*
    This controller method to set ComputerLanguage model with mock content. Ideally, fetch data from repository (DB or Webservices)
     */
    public ComputerLanguage setText(){
        return new ComputerLanguage("Java","Android");
    }

    /*
    This controller method will be invoked from Button Event
     */
    public void displayText(){
        tvDisplayArea.setText(setText().getLanguage());
    }
}